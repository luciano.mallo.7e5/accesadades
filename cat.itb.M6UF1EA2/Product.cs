using System;

namespace cat.itb.M6UF1EA2
{
    public class Product
    {
        public string name { get; set; }
        public decimal price { get; set; }
        public int stock { get; set; }
        public string picture { get; set; }
        public string[] categories { get; set; }

        /*public override string ToString()
        {
            return "name: " + name + "  price: " + price + "   stock: " + stock + "   picture: " + picture+ "   categories: " +
                   categories;
        }*/

    }



}