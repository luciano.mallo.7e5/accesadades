﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace cat.itb.M6UF1EA2
{
    public class Program
    {
        public static void Main()
        {
            Inici();
        }

        public static void Inici()

        {
            bool fi;
            do
            {
                MostrarMenu();
                fi = TratarOpcion();
            } while (!fi);
        }

        static void MostrarMenu()
        {
            Console.WriteLine("\n Treball amb Arxius de Text");

            Console.WriteLine("-------------------------------------");
            Console.WriteLine("[0] Salir.");
            Console.WriteLine("[1] Ejercicio 1.");
            Console.WriteLine("[2] Ejercicio 2.");
            Console.WriteLine("[3] Ejercicio 3.");
        }

        static bool TratarOpcion()

        {
            string opcion = Console.ReadLine();

            switch (opcion)

            {
                case "0":

                    return true;

                case "1":

                    Ejercicio1();

                    break;
                case "2":
                    Ejercicio2();
                    break;

                case "3":
                    Ejercicio3();
                    break;
                case "4":
                    Ejercicio3();
                    break;
                case "5":
                    Ejercicio3();
                    break;
                case "6":
                    Ejercicio3();
                    break;
                case "7":
                    Ejercicio3();
                    break;
                case "8":
                    Ejercicio3();
                    break;
                case "9":
                    Ejercicio3();
                    break;
                case "10":
                    Ejercicio3();
                    break;
                

                default:

                    Console.WriteLine("Opció incorrecta!\n");
                    break;
            }

            return false;
        }


        public static void Ejercicio1()
        {
            List<Product> product1 = new List<Product>();
            string path = @"../../../MyFiles/products1.json";
            using (StreamReader jsonStream = File.OpenText(path))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    Product product = JsonConvert.DeserializeObject<Product>(line);
                    product1.Add(product);
                }
            }

            foreach (Product product in product1)
            {
                if (product.price > 600)
                {
                    Console.WriteLine(product.name);
                }
            }
        }

       public static void Ejercicio2()
        {
            List<Product> product1 = new List<Product>();
            List<Product> product2 = new List<Product>();
            string path = @"../../../MyFiles/products1.json";
            //Creo la lista 1 para recorrerla y modificar el elemento dentro del array
            using (StreamReader JsonStream = File.OpenText(path))
            {
                string line;
                while ((line = JsonStream.ReadLine()) != null)
                {
                    Product product = JsonConvert.DeserializeObject<Product>(line);
                    product1.Add(product);
                }
            }

            Console.WriteLine("\nFichero Original\n");
            //Recorro la lista buscando el elemento a cambiar y lo muestro.
                foreach (Product product in product1)
                {
                    if (product.name=="iPhone 8")
                    {
                        string json3 = JsonConvert.SerializeObject(product);
                        Console.WriteLine(json3);
                    }
                } 
            //Recorro la lista buscando el elemento a cambiar, solos seriabilizo el elemento que modifico.
                foreach (Product product in product1)
                {
                    if (product.name=="iPhone 8")
                    {
                        string[] categories =  product.categories;
                        Array.Resize(ref categories,categories.Length+1);
                        categories[categories.Length - 1] = "supersmartphone";
                        product.categories = categories;
                    }
                    
                }
                //Escribo el fichero de nuevo con el elemento modificado.
                using (StreamWriter sw = new StreamWriter(path))
                {
                    foreach (Product product in product1)
                    {
                        string json = JsonConvert.SerializeObject(product);
                        sw.WriteLine(json);
                    }
                }

                Console.WriteLine("\nJSON file changed\n");
                //Abro el fichero de nuevo y lo guardo en una nueva lista.
                using (StreamReader JsonStream = File.OpenText(path))
                {
                    string line;
                    while ((line = JsonStream.ReadLine()) != null)
                    {
                        Product product = JsonConvert.DeserializeObject<Product>(line);
                        product2.Add(product);
                    }
                }
                //Recorro la lista para mostrar el elemento cambiado.
                foreach (Product product in product2)
                {
                    if (product.name=="iPhone 8")
                    {
                        string json3 = JsonConvert.SerializeObject(product);
                        Console.WriteLine(json3);
                    }
                }

            }
       

        public static void Ejercicio3()
        {
            /*3.- Modifica el stock del producte amb nom MacBook del fitxer products1.json. Ara el nou stock d’aquest
                producte és 5.*/
            List<Product> product1 = new List<Product>();
            List<Product> product2 = new List<Product>();
            string path = @"../../../MyFiles/products1.json";
            //Creo la lista 1 para recorrerla y la lleno
            using (StreamReader JsonStream = File.OpenText(path))
            {
                string line;
                while ((line = JsonStream.ReadLine()) != null)
                {
                    Product product = JsonConvert.DeserializeObject<Product>(line);
                    product1.Add(product);
                }
            }
            //recorro la lista 1 y muestro el elemento antes de modificarlo
            foreach (Product product in product1)
            {
                if (product.name=="MacBook")
                {
                    string json3 = JsonConvert.SerializeObject(product);
                    Console.WriteLine(json3);
                }
            } 
            //Vuelvo a recorrer y modifico el elemento.
            foreach (var product in product1)
            {
                if (product.name=="MacBook")
                {
                    product.stock = 5;
                }
            }
            //Vuelvo a escribir el documento con el elemento cambiado.
            using (StreamWriter sw=new StreamWriter(path))
            {
                foreach (Product product in product1)
                {
                    string json = JsonConvert.SerializeObject(product);
                    sw.WriteLine(json);
                }
                
            }
            //Abro el documento y lo guardo en la lista 2
            using (StreamReader jsonStream = File.OpenText(path) )
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    Product product = JsonConvert.DeserializeObject<Product>(line);
                    product2.Add(product);
                }
            }
            //Recorro la lista 2 y muestro el elemento cambiado.
            foreach (Product product in product2)
            {
                if (product.name=="MacBook")
                {
                    string json3 = JsonConvert.SerializeObject(product);
                    Console.WriteLine(json3);
                }
                
            }
            
            
            
        }


        public static void Ejercicio4()
        {
        }
    }
}